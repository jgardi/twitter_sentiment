import tweepy
from functools import cached_property
import numpy as np
from datetime import datetime
import os
import pkg_resources
from typing import Generator, Tuple, List
import joblib

tweet_cache = joblib.Memory("~/.twitter_sentiment")


def epoch_time(dt: datetime):
    return int((dt - datetime(1970, 1, 1)).total_seconds() * 1000)


class TwitterHelper:
    def __init__(self):
        self.tweets_between_dates = tweet_cache.cache(self.tweets_between_dates)

    @cached_property
    def api(self):
        # ACCESS_TOKEN = "2599322580-bV72AUbDWJQHqqWcYYPHxckhjQ6QmdYFfhqIrLA"
        ACCESS_TOKEN = "1272933127212666883-fYL5t7AZMKvAaPwvGEwtVNK4ReDC64"
        # ACCESS_TOKEN_SECRET = "bX3nrNnGhYYt2a19OiCUtz6LtO5qQBIhtp3fsPNiwzO0U"
        ACCESS_TOKEN_SECRET = "Gfz5gHI6pz1E0Tj9WUzYhYvTqoUPwJuPSTpnvb9XRypB5"
        # CONSUMER_KEY = "RLytuOpmJUxPvpGHqn4Wr5qqE"
        CONSUMER_KEY = "OEBQJwD9tPLmaa5xSLOiBSejM"
        # CONSUMER_SECRET = "97xrCDEQRXCuhGtbLgfg3UyUExlePeeJcADnGmKdbYO6usfsRK"
        CONSUMER_SECRET = "ZKcuEB1xNR6rT2ffsMUvJhnN7o06dFyTLbETCMwUwBhfnEmQ0k"
        auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

        return tweepy.API(auth, wait_on_rate_limit=True)

    def rand_tweets_iter(self):
        public_tweets = tweepy.Cursor(
            self.api.user_timeline, screen_name="@realDonaldTrump"
        ).pages()
        for page in public_tweets:
            for tweet in page:
                yield tweet

    def tweet_times_and_ids(self) -> Tuple[List[int], List[int]]:
        tweet_times = []
        tweet_ids = []
        for tweet in self.rand_tweets_iter():
            tweet_times.append(epoch_time(tweet.created_at))
            tweet_ids.append(tweet.id)

        return list(reversed(tweet_times)), list(reversed(tweet_ids))

    @cached_property
    def date_to_id_model(self):
        proj_root = "/".join(pkg_resources.normalize_path("").split("/")[:-1])
        model_path = f"{proj_root}/models/date_to_id_model.npy"
        print(model_path)
        if not os.path.exists(model_path):
            tweet_times, tweet_ids = self.tweet_times_and_ids()
            times_arr = np.array(tweet_times)
            coefs = np.polyfit(times_arr, tweet_ids, 5)
            np.save(model_path, coefs)
        else:
            coefs = np.load(model_path)

        return np.poly1d(coefs)

    def tweets_between_dates(
        self, start: datetime, end: datetime, users: List[str]
    ) -> list:
        tweets = []
        print("get tweets")
        for user in users:
            since_id = int(self.date_to_id_model(epoch_time(start)))
            max_id = int(self.date_to_id_model(epoch_time(end)))
            public_tweets = tweepy.Cursor(
                self.api.user_timeline,
                screen_name=user,
                since_id=since_id,
                max_id=max_id,
                result_type="recent",
            ).pages()
            try:
                tweets.extend(get_tweets(public_tweets, start))
            except Exception as e:
                print("error", e)

        return tweets


def get_tweets(pages, since):
    for page in pages:
        print("page loaded")
        for tweet in page:
            # print("done")
            if tweet.created_at < since:
                return
            yield tweet


twitter_helper = TwitterHelper()
