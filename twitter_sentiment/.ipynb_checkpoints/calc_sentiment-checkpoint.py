from twitter_sentiment.TwitterHelper import twitter_helper
from textblob import TextBlob


def get_avg_sentiment(start, end, users):
    tweets = twitter_helper.tweets_between_dates(start, end, users)
    tot_sentiment = 0
    count = 0
    for tweet in tweets:
        count += 1
        tot_sentiment += TextBlob(tweet.text).sentiment.polarity
    if count == 0:
        return 0
    else:
        return tot_sentiment / count
