# Installation
Run the following. The -e flag makes it so that changes should take affect across the environment without having to re install.
```
pip install -e .
```

# Formatting
Use black formatting for Python.

# Ideas for improvement
Even defining what the the sentiment should be is tricky. Tbere are labeled datasets but the labaels are 
somewhat arbitrary. One approach would be to ensemble several methods trained on different datasets or based on 
different hand coded rules so that you can "diversify away" bias in any one method.

It may be helpful to come up with a new definition of sentiment that accounts for differences in overall tones of accounts.
Rather than looking at absolute sentiment, we look at sentiment relative to what we normally see from some user. 
Some users will simply tend to be more optimistic than others. 

