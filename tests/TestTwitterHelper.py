import unittest
from twitter_sentiment.TwitterHelper import twitter_helper
from twitter_sentiment import calc_sentiment
from datetime import datetime, timedelta


class TestTwitterHelper(unittest.TestCase):
    def test_date_to_id_model(self):
        result = twitter_helper.date_to_id_model()

    def test_tweets_between_dates(self):
        print(
            calc_sentiment.get_avg_sentiment(
                datetime.now() - timedelta(days=1),
                datetime.now(),
                ["@jimcramer", "@Benzinga"],
            )
        )


if __name__ == "__main__":
    unittest.main()
